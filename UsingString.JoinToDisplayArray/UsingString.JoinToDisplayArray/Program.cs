﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingString.JoinToDisplayArray
{
    class Program
    {
        static void Main(string[] args)
        {
            //Store of variables
            string[] colours = new string[5] { "red", "blue", "orange", "black", "white" };
            string name;

            //Explanation to the user
            Console.WriteLine("Hi there, what is your name user?");
            name = Console.ReadLine();
            Console.WriteLine("Thanks {0}, this program here is going to join the strings of an array.", name);
            Console.WriteLine("First, lets see the array.");
            Console.WriteLine();
            //Creates variable (i) and stores the value of colours
            foreach (string i in colours)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine();
            Console.WriteLine("Cool isn't it {0}, now lets see what happens when we link them together.", name);
            Console.WriteLine();
            //Joining the strings of the array together
            Console.WriteLine(string.Join(", ", colours));
        }
    }
}
